from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Category, Post

from post.serializers import CategorySerializer


CATEGORIES_URL = reverse('post:category-list')


class PublicCategorysApiTests(TestCase):
    """Test the publicly available categories API"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required to access the endpoint"""
        res = self.client.get(CATEGORIES_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateCategorysApiTests(TestCase):
    """Test the private categories API"""

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@django.com',
            'testpass'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_category_list(self):
        """Test retrieving a list of categories"""
        Category.objects.create(user=self.user, name='Kale')
        Category.objects.create(user=self.user, name='Salt')

        res = self.client.get(CATEGORIES_URL)

        categories = Category.objects.all().order_by('-name')
        serializer = CategorySerializer(categories, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_categories_limited_to_user(self):
        """Test that categories for the authenticated user are returend"""
        user2 = get_user_model().objects.create_user(
            'other@django.com',
            'testpass'
        )
        Category.objects.create(user=user2, name='Education')
        category = Category.objects.create(user=self.user, name='Programming')

        res = self.client.get(CATEGORIES_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], category.name)

    def test_create_category_successful(self):
        """Test create a new category"""
        payload = {'name': 'Cabbage'}
        self.client.post(CATEGORIES_URL, payload)

        exists = Category.objects.filter(
            user=self.user,
            name=payload['name'],
        ).exists()
        self.assertTrue(exists)

    def test_create_category_invalid(self):
        """Test creating invalid category fails"""
        payload = {'name': ''}
        res = self.client.post(CATEGORIES_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_categories_assigned_to_posts(self):
        """Test filtering categories by those assigned to posts"""
        category1 = Category.objects.create(
            user=self.user, name='Blabla'
        )
        category2 = Category.objects.create(
            user=self.user, name='Turkey'
        )
        post = Post.objects.create(
            title='Apple crumble',
            time_minutes=5,
            price=10,
            user=self.user
        )
        post.categories.add(category1)

        res = self.client.get(CATEGORIES_URL, {'assigned_only': 1})

        serializer1 = CategorySerializer(category1)
        serializer2 = CategorySerializer(category2)
        self.assertIn(serializer1.data, res.data)
        self.assertNotIn(serializer2.data, res.data)

    def test_retrieve_categories_assigned_unique(self):
        """Test filtering categories by assigned returns unique items"""
        category = Category.objects.create(user=self.user, name='Eggs')
        Category.objects.create(user=self.user, name='Cheese')
        post1 = Post.objects.create(
            title='Eggs benedict',
            time_minutes=30,
            price=12.00,
            user=self.user
        )
        post1.categories.add(category)
        post2 = Post.objects.create(
            title='Coriander eggs on toast',
            time_minutes=20,
            price=5.00,
            user=self.user
        )
        post2.categories.add(category)

        res = self.client.get(CATEGORIES_URL, {'assigned_only': 1})

        self.assertEqual(len(res.data), 1)
